<?php

namespace CQM\Libraries\Security\Tests;

use PHPUnit\Framework\TestCase;
use CQM\Libraries\Security\Signature;

class SignatureTest extends TestCase
{
    /** @test */
    public function nonceDoesNotContainSlashes()
    {
        Signature::makeHmacSha256('1234', $n, $t);

        $this->assertFalse(strpos($n, "/"));
        $this->assertFalse(strpos($n, "\\"));
    }
}