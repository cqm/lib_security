<?php

namespace CQM\Libraries\Security;

class Signature
{
    const BYTES_LENGTH = 8;
    const STRIP_REGEX = array("~/~", "/~\\~/"); 
    const REPLACE_WITH = "X";

    public static function makeHmacSha256($secret, &$nonce = null, &$ts = null)
    {
        if ($ts === null) {
            $ts = time();
        }

        if ($nonce === null) {
            $nonce = self::stripSlashes(base64_encode(bin2hex(openssl_random_pseudo_bytes(self::BYTES_LENGTH))));
        }

        return hash_hmac('sha256', $nonce . '#' . $ts, $secret);
    }

    private static function stripSlashes($string = "")
    {
        return \preg_replace(
            self::STRIP_REGEX,
            self::REPLACE_WITH,
            $string
        );
    }
}
